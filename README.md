## Focus API sync

### Description

TODO

### Development

1. Fork repository

1. Clone forked repository

        $ git clone git@bitbucket.org:<your_account_name>/focus-pos-sync.git

1. Add upstream repo

        $ git remote add upstream git@bitbucket.org:whatsbusy/focus-pos-sync.git

1. Fetch upstream repo

        $ git fetch upstream

1. Create development branch.

        $ git checkout upstream/master
        $ git chechkoug -b <task_name>

    `<task_name>` should be Trello card ID like: `1-my-first-task`

1. Add changed files to index

        $ git add <file>

1. Check index files before commit

        $ git status

1. Commit changes:

        $ git commit -m'<task_name>: <task description>'

    For example:

        $ git commit -m'1-my-first-task: test fixups'

1. Push your branch to `origin` repository:

        $ git push origin <branch_name>

    For example:

        $ git push origin 1-my-first-task

1. Create pull request
