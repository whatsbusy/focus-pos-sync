import setuptools

setuptools.setup(
    name="focus_pos_sync",
    version="0.0.5",
    packages=setuptools.find_packages(),
    install_requires=[
        "requests==2.20.1",
        "boto3==1.9.48",
        "click==7.0",
        "backoff==1.7.0"
    ],
    tests_require=[
        "nose",
        "httpretty==0.9.6"
    ],
    entry_points='''
        [console_scripts]
        focus_pos_sync=focus_pos_sync.bin.focus_pos_sync:cli
    '''
)
