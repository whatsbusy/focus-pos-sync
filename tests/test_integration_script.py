import io
import unittest
from unittest import mock

from focus_pos_sync.integration_script import get_workers, _parse_config_file_content


class IntegrationScriptTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.config_storage = mock.MagicMock()
        cls.upload_storage = mock.MagicMock()
        cls.config_path = 'config.csv'
        cls.line1 = '123456,1,2018-01-01'
        cls.line2 = '654321,2,2018-01-01'
        cls.file = 'pos_id,store_key,start_date\n%s\n%s' % (cls.line1, cls.line2)

        cls.config_storage.download = mock.MagicMock(
            return_value=io.StringIO(cls.file))

        cls.workers = get_workers(
            cls.config_storage, cls.config_path, cls.upload_storage,
            'path', '123456'
        )

    def test_parse_config_file_content(self):
        """Test parse config file method with namedtuple"""
        parsed_content = list(_parse_config_file_content(io.StringIO(self.file)))

        parsed_line1 = '%s,%s,%s' % (
            parsed_content[0].pos_id, parsed_content[0].store_key, parsed_content[0].start_date)
        parsed_line2 = '%s,%s,%s' % (
            parsed_content[1].pos_id, parsed_content[1].store_key, parsed_content[1].start_date)
        self.assertEqual(self.line1, parsed_line1)
        self.assertEqual(self.line2, parsed_line2)

    def test_get_workers_with_returned_2_workers(self):
        """Test get_workers method with 2 workers"""
        workers = list(self.workers)
        self.assertEqual(len(workers), 2)
        self.config_storage.download.assert_called_once_with(self.config_path)
        self.assertEqual(workers[0].upload_path, 'path/123456')
        self.assertEqual(workers[1].upload_path, 'path/654321')
