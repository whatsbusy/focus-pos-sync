import unittest
import requests
import httpretty
import io
from test.support import EnvironmentVarGuard

from focus_pos_sync.api_client import FocusPOSAPIClient


class FocusPOSAPIClientTests(unittest.TestCase):

    def _generate_url(self, api_client):
        return '%sapi/data/xml' % api_client.base_url

    def test_url_default_value(self):
        """Test url for default value"""
        api_client = FocusPOSAPIClient('123456')

        self.assertEqual(api_client.base_url, 'https://report.api.focusca.net/')

    def test_url_from_env(self):
        """Test url for env value"""
        url = 'https://api.example.com/'

        with EnvironmentVarGuard() as env:
            env['FOCUS_API_BASE'] = url

            api_client = FocusPOSAPIClient('123456')
            self.assertEqual(api_client.base_url, url)

    def test_headers(self):
        """Test headers for api client"""
        api_client = FocusPOSAPIClient('123456')

        self.assertEqual(
            api_client.http_headers(), {'Authorization': 'Bearer 123456'})

    @httpretty.activate
    def test_request_500_token_not_valid(self):
        """Test request for api client, token not valid"""
        token = '123456'
        api_client = FocusPOSAPIClient(token, ignore_server_errors=False)

        httpretty.register_uri(
            httpretty.GET,
            self._generate_url(api_client),
            status=500,
            forcing_headers={"Authorization": "Bearer %s" % token}
        )
        with self.assertRaises(requests.exceptions.HTTPError):
            api_client.get_store_daily_data(data={})

    @httpretty.activate
    def test_request_500_ignored(self):
        """Test request for api client, token not valid"""
        token = '123456'
        api_client = FocusPOSAPIClient(token, ignore_server_errors=True)

        httpretty.register_uri(
            httpretty.GET,
            self._generate_url(api_client),
            status=500,
            forcing_headers={"Authorization": "Bearer %s" % token}
        )
        self.assertIsNone(api_client.get_store_daily_data(data={}))

    @httpretty.activate
    def test_request_200_success(self):
        """Test request for api client, 200 status success"""
        token = '123456'
        api_client = FocusPOSAPIClient(token)

        body = 'test data'
        httpretty.register_uri(
            httpretty.GET,
            self._generate_url(api_client),
            status=200,
            forcing_headers={"Authorization": "Bearer %s" % token},
            body=body
        )

        file = api_client.get_store_daily_data(data={})
        self.assertEqual(type(file), io.StringIO)
        self.assertEqual(file.read(), body)

    @httpretty.activate
    def test_request_200_success_with_binary_file(self):
        """Test request for api client, 200 status success with binary file"""
        token = '123456'
        api_client = FocusPOSAPIClient(token)

        httpretty.register_uri(
            httpretty.GET,
            self._generate_url(api_client),
            status=200,
            forcing_headers={"Authorization": "Bearer %s" % token},
            body='test data'
        )

        file = api_client.get_store_daily_data(data={}, binary=True)
        self.assertEqual(type(file), io.BytesIO)
        self.assertEqual(file.read(), b'test data')

    @httpretty.activate
    def test_request_404_success(self):
        """Test request for api client, 404 status success"""
        token = '123456'
        api_client = FocusPOSAPIClient(token)

        httpretty.register_uri(
            httpretty.GET,
            self._generate_url(api_client),
            status=404,
            forcing_headers={"Authorization": "Bearer %s" % token}
        )

        self.assertEqual(api_client.get_store_daily_data(data={}), None)


if __name__ == '__main__':
    unittest.main()
