import unittest
from unittest import mock
from datetime import datetime, date

from focus_pos_sync import storage
from focus_pos_sync.worker import DownloadWorker


def _generate_url(api_client):
    return '%sapi/data/xml' % api_client.url


class DownloadWorkerTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.upload_path = '07175e0b-f44e-4f2f-80d4-941c2f604d09'

    def test_get_dates_to_download_one_date(self):
        """Test date range with one date, list called to check if file exist"""
        api_client = mock.MagicMock()
        fs = mock.MagicMock()
        dw = DownloadWorker(
            upload_storage=fs, api_client=api_client, upload_path=self.upload_path,
            store_key='2', start_date=date(2018, 2, 1),
            end_date=date(2018, 2, 1)
        )

        self.assertEqual(
            list(dw.get_dates_to_download()),
            [date(2018, 2, 1)]
        )

        fs.list.assert_called_once_with('07175e0b-f44e-4f2f-80d4-941c2f604d09/')

    def test_get_dates_to_download(self):
        """Test date range, list called to check if file exist"""
        api_client = mock.MagicMock()
        fs = mock.MagicMock()
        dw = DownloadWorker(
            upload_storage=fs, api_client=api_client,
            upload_path=self.upload_path, store_key='2', start_date=date(2018, 2, 1),
            end_date=date(2018, 2, 5)
        )

        self.assertEqual(
            dw.get_dates_to_download(),
            [
                date(2018, 2, 5),
                date(2018, 2, 4),
                date(2018, 2, 3),
                date(2018, 2, 2),
                date(2018, 2, 1)
            ]
        )

        fs.list.assert_called()

    def test_get_dates_to_download_one_date_already_downloaded(self):
        """Test dates to download without 2018-02-03 date because it already exists"""
        temp_dir = 'test'
        new_file_name = '%s/2018-02-03.xml' % temp_dir
        storage.list = mock.Mock(return_value=[new_file_name])
        fs = storage

        dw = DownloadWorker(upload_storage=fs, api_client=None, upload_path=temp_dir, store_key='2', start_date=date(2018, 2, 1), end_date=date(2018, 2, 5))

        self.assertEqual(
            dw.get_dates_to_download(),
            [
                date(2018, 2, 5),
                date(2018, 2, 4),
                date(2018, 2, 2),
                date(2018, 2, 1)
            ]
        )

    def test_generate_params(self):
        """Test generate params"""
        dw = DownloadWorker(
            None, None, self.upload_path, '2', datetime(2018, 2, 5).date(),
            datetime(2018, 2, 5).date()
        )

        self.assertEqual(
            dw._generate_params(datetime(2018, 2, 5).date()),
            {'StoreKey': '2', 'CheckDate': '2018-02-05'}
        )

    def test_generate_key(self):
        """Test generate key (dir/file.xml)"""
        dw = DownloadWorker(
            None, None, self.upload_path, '2', datetime(2018, 2, 5).date(),
            datetime(2018, 2, 5).date()
        )

        self.assertEqual(
            dw._generate_key(datetime(2018, 2, 5).date()),
            '07175e0b-f44e-4f2f-80d4-941c2f604d09/2018-02-05.xml'
        )

    def test_run_called_with_one_date(self):
        """Test worker with one date"""
        api_client = mock.MagicMock()
        fs = mock.MagicMock()

        dw = DownloadWorker(
            upload_storage=fs, api_client=api_client,
            upload_path=self.upload_path, store_key='2', start_date=date(2018, 2, 1),
            end_date=date(2018, 2, 1)
        )

        dw.run()

        api_client.get_store_daily_data.assert_called_once()
        fs.list.assert_called_once()


if __name__ == '__main__':
    unittest.main()
