import io
import unittest
from unittest import mock
from tempfile import TemporaryDirectory

from focus_pos_sync.storage import FilesystemStorage, S3Storage


class FilesystemStorageTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.temp_dir = TemporaryDirectory()
        cls.files = []
        for i in range(5):
            with open('%s/%s.txt' % (cls.temp_dir.name, i), "w") as file:
                file.write('Content of %s' % file.name)
                cls.files.append(file.name)

    @classmethod
    def tearDownClass(cls):
        cls.temp_dir.cleanup()

    def test_filesystem_list(self):
        """Test for listing filesystem dir"""
        fs = FilesystemStorage()
        fs_list = fs.list(self.temp_dir.name)

        self.assertEqual(len(fs_list), len(self.files))
        self.assertSetEqual(set(fs.list(self.temp_dir.name)), set(self.files))

    def test_filesystem_download(self):
        """Test for downloading file from filesystem dir"""
        fs = FilesystemStorage()
        file_to_download = self.files[1]
        fs_file = fs.download(file_to_download)

        self.assertEqual(fs_file.read(), 'Content of %s' % file_to_download)

    def test_filesystem_download_binary(self):
        """Test for downloading file from filesystem dir with binary option"""
        fs = FilesystemStorage(binary=True)
        file_to_download = self.files[1]
        fs_file = fs.download(file_to_download)
        file_content = fs_file.read()

        self.assertEqual(type(file_content), bytes)
        self.assertEqual(
            file_content,
            str.encode('Content of %s' % file_to_download)
        )

    def test_filesystem_upload(self):
        """Test for uploading file to filesystem dir"""
        fs = FilesystemStorage()

        file_content = b'test test test'

        new_file_name = '%s/10.txt' % self.temp_dir.name
        with io.BytesIO(file_content) as temp_file:
            fs.upload(temp_file, new_file_name)

        with open(new_file_name, 'rb') as opened_file:
            self.assertEqual(opened_file.read(), file_content)


class S3StorageTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.s3_client = mock.MagicMock()
        cls.bucket = 'mybucket'

    def test_s3_list(self):
        s3_storage = S3Storage(self.s3_client, self.bucket)
        s3_storage.list('test.txt')

        self.s3_client.list_objects.assert_called_once_with(
            Bucket=self.bucket, Prefix='test.txt')

    def test_s3_download(self):
        s3_storage = S3Storage(self.s3_client, self.bucket)
        file_name = 'test.txt'
        s3_storage.download(file_name)

        self.s3_client.download_fileobj.assert_called_once_with(
            Bucket=self.bucket, Key=file_name, Fileobj=mock.ANY)

    def test_s3_upload(self):
        s3_storage = S3Storage(self.s3_client, self.bucket)

        file_content = 'test test test'

        new_file_name = 'test.txt'
        with io.StringIO(file_content) as temp_file:
            s3_storage.upload(temp_file, new_file_name)

        self.s3_client.upload_fileobj.assert_called_once_with(
            Bucket=self.bucket, Key=new_file_name, Fileobj=temp_file)
