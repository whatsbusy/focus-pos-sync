#!/usr/bin/env python3
import os
import sys
import click
import logging
from urllib.parse import urlparse

from focus_pos_sync.storage import get_storage, get_key
from focus_pos_sync.integration_script import get_workers, ThreadPoolWorkerWrapper
from focus_pos_sync.errors import ConfigurationError

logger = logging.getLogger(__name__)


def set_logger(level):
    root_logger = logging.getLogger()
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s : %(levelname)s : %(threadName)s: %(message)s')
    handler.setFormatter(formatter)
    root_logger.addHandler(handler)
    root_logger.setLevel(level)

    return root_logger


@click.command()
@click.option('--token', '-t', help='Focus API Bearer token')
@click.option('--file', '-f', help='Input CSV config file. Names started with s3:// are considered to be S3 paths. '
                                   'Local filesystem is implied otherwise.\n'
                                   'For example:\n'
                                   '    s3://input-bucket/stores.csv - read from S3\n'
                                   '    ./stores.csv - read from FileSystem')
@click.option('--output', '-o', help='Output directory. Names started with s3:// are considered to be S3 paths.'
                                     ' Local filesystem is implied otherwise.\n'
                                     'For example:\n'
                                     '    s3://output-bucket/prefix-path - ouput to S3\n'
                                     '    ./output-data - ouput to FileSystem')
@click.option('--aws-key', help='AWS Access Key')
@click.option('--aws-secret', help='AWS Secret Access Key')
@click.option('--jobs', '-j', help='Number of workers (jobs) running in parallel. Default: 1')
@click.option('--log-level', '-l', help='Log level. Defaults: INFO')
def cli(token, file, output, aws_key, aws_secret, jobs, log_level):
    set_logger(log_level or os.environ.get('FOCUS_LOG_LEVEL') or 'INFO')
    input_path = file or os.environ.get('FOCUS_INPUT')
    output_path = output or os.environ.get('FOCUS_OUTPUT')
    num_workers = int(jobs or os.environ.get('FOCUS_JOBS') or 1)
    if not input_path:
        raise ConfigurationError('Input path was not specified.')
    if not output_path:
        raise ConfigurationError('Output path was not specified.')

    input_storage = get_storage(
        file_string=input_path,
        aws_key=aws_key, aws_secret=aws_secret
    )
    output_storage = get_storage(
        file_string=output_path,
        aws_key=aws_key, aws_secret=aws_secret
    )

    workers = get_workers(config_storage=input_storage, config_path=get_key(input_path),
                          upload_storage=output_storage, root_upload_path=get_key(output_path),
                          token=token)
    ThreadPoolWorkerWrapper(workers=workers, thread_pool_size=num_workers).run()
