import os
import logging
from operator import itemgetter
from urllib.parse import urlparse
from tempfile import NamedTemporaryFile

import boto3
import botocore
import backoff

from .errors import giveup_handler
from .config.backoff import retry_timeout_sec, max_retries

logger = logging.getLogger(__name__)


def get_storage(file_string, aws_key=None, aws_secret=None):
    file_parser = urlparse(file_string)
    if file_parser.scheme == 's3':
        client = boto3.client(
            's3',
            aws_access_key_id=aws_key,
            aws_secret_access_key=aws_secret
        )
        return S3Storage(client, file_parser.netloc)
    else:
        return FilesystemStorage()


def get_key(file_string):
    file_parser = urlparse(file_string)
    if file_parser.scheme == 's3':
        return file_parser.path.lstrip('/')
    return file_string


class Storage(object):

    def list(self, key):
        """
        Returns a list of files
        """
        raise NotImplementedError()

    def download(self, key):
        """
        Downloads file from storage and returns file-like object
        """
        raise NotImplementedError()

    def upload(self, file, key):
        """
        Uploads file-like object f to storage
        """
        raise NotImplementedError()


class FilesystemStorage(Storage):

    def __init__(self, binary=False):
        self.binary = binary

    def _walk_files(self, top):
        for dirpath, _, filenames in os.walk(top):
            for filename in filenames:
                yield os.path.join(dirpath, filename)

    def list(self, key):
        logger.info('Listing files from %s.' % key)
        return list(self._walk_files(key))

    def download(self, key):
        logger.info('Opening file from %s.' % key)
        options = 'rb' if self.binary else 'r'
        return open(key, options)

    def upload(self, file, key):
        logger.info('Saving data to %s.' % key)
        os.makedirs(os.path.dirname(key), exist_ok=True)
        with open(key, 'wb') as out_file:
            out_file.write(file.read())


class S3Storage(Storage):

    def __init__(self, s3_client, bucket, binary=False):
        self.s3_client = s3_client
        self.bucket = bucket
        self.binary = binary

    @backoff.on_exception(
        backoff.expo,
        botocore.exceptions.ClientError,
        max_time=retry_timeout_sec,
        max_tries=max_retries,
        on_backoff=giveup_handler)
    def list(self, key):
        logger.info('Listing files from %s bucket and %s.' % (self.bucket, key))
        objs = self.s3_client.list_objects(
            Bucket=self.bucket, Prefix=key)

        if 'Contents' not in objs:
            return []

        keys = map(itemgetter('Key'), objs['Contents'])
        return list(filter(lambda k: not k.endswith('/'), keys))

    @backoff.on_exception(
        backoff.expo,
        botocore.exceptions.ClientError,
        max_time=retry_timeout_sec,
        max_tries=max_retries,
        on_backoff=giveup_handler)
    def download(self, key):
        logger.info('Downloading file from %s bucket and %s.' % (self.bucket, key))
        with NamedTemporaryFile(mode='wb', delete=False) as temp_file:
            self.s3_client.download_fileobj(
                Bucket=self.bucket, Key=key, Fileobj=temp_file)

        r_options = 'rb' if self.binary else 'r'
        return open(temp_file.name, mode=r_options)

    def upload(self, file, key):
        # TODO: binary mode file is expected. Implement non-binary file support
        logger.info('Uploading file to %s bucket and %s.' % (self.bucket, key))
        self.s3_client.upload_fileobj(
            Bucket=self.bucket,
            Key=key,
            Fileobj=file)
