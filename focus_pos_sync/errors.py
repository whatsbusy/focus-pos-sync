import logging

logger = logging.getLogger(__name__)


class ConfigurationError(Exception):
    pass


def giveup_handler(e):
    logger.warning('Error raised for request: %s. ' % str(e))

    return True
