import io
import os
import requests
import logging
from urllib import parse

from .errors import ConfigurationError

logger = logging.getLogger(__name__)


class FocusPOSAPIClient(object):

    def __init__(self, token, ignore_server_errors=False):
        self.token = token or os.environ.get('FOCUS_API_TOKEN')
        self.ignore_server_errors = ignore_server_errors
        self.base_url = os.environ.get(
            'FOCUS_API_BASE', 'https://report.api.focusca.net/')

    def http_headers(self):
        if self.token is None:
            raise ConfigurationError('Auth token was not specified.')
        return {
            'Authorization': 'Bearer %s' % self.token
        }

    def get_store_daily_data(self, data, binary=False):
        logger.info('Getting data from %s with data: %s.' % (self.base_url, data))
        r = requests.get(
            parse.urljoin(base=self.base_url, url='api/data/xml'),
            params=data,
            headers=self.http_headers()
        )

        if r.status_code == 200:
            return io.BytesIO(r.content) if binary else io.StringIO(r.text)
        elif r.status_code == 404:
            logger.warning('Response data for params %s not found', data)
            return None
        elif r.status_code == 500 and self.ignore_server_errors:
            logger.warning('Server error occured, params %s', data)
        else:
            r.raise_for_status()
