import os
import csv
import logging
import concurrent.futures
from datetime import datetime
from typing import List
from collections import namedtuple

from .api_client import FocusPOSAPIClient
from .worker import Worker, DownloadWorker

logger = logging.getLogger(__name__)
ConfigRow = namedtuple('ConfigRow', ['pos_id', 'store_key', 'start_date'])


class ThreadPoolWorkerWrapper(Worker):

    def __init__(self, workers: List[DownloadWorker], thread_pool_size=1):
        self.workers = list(workers)
        self.thread_pool_size = thread_pool_size

    def run(self):
        logger.info('ThreadPoolWorker initialized with %s thread pool size for %s workers.' % (
            self.thread_pool_size, len(self.workers)))
        with concurrent.futures.ThreadPoolExecutor(max_workers=self.thread_pool_size) as executor:
            futures = [executor.submit(w.run) for w in self.workers]
            for i, future in enumerate(concurrent.futures.as_completed(futures)):
                try:
                    future.result()
                except Exception as exc:
                    logger.exception('Worker %s failed: %s', i, exc)
                else:
                    logger.info('Worker %s has finished', i)


def _parse_config_file_content(config_file):
    file_content = csv.DictReader(config_file)

    for row in file_content:
        yield ConfigRow(pos_id=row['pos_id'], store_key=row['store_key'], start_date=row['start_date'])


def get_workers(config_storage, config_path, upload_storage, root_upload_path, token,
                ignore_server_errors=False):
    config_file = config_storage.download(config_path)
    logger.info('Parsing config file from %s.' % config_path)
    for row in _parse_config_file_content(config_file):
        focus_api_client = FocusPOSAPIClient(token=token, ignore_server_errors=ignore_server_errors)
        pos_id = row.pos_id
        yield DownloadWorker(
            upload_storage=upload_storage, api_client=focus_api_client,
            upload_path=os.path.join(root_upload_path, pos_id), store_key=row.store_key,
            start_date=datetime.strptime(row.start_date, '%Y-%m-%d').date()
        )
