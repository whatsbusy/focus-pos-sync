import os
import re
import logging
from datetime import datetime, timedelta

import requests
import backoff

from .errors import giveup_handler
from .config.backoff import retry_timeout_sec, max_retries

FILE_PATTERN_RE = re.compile(r'.*/(?P<date>\d{4}-\d{2}-\d{2}).xml$')
logger = logging.getLogger(__name__)


class Worker(object):
    def run(self):
        raise NotImplementedError()


class DownloadWorker(Worker):
    date_format = '%Y-%m-%d'

    def __init__(self, upload_storage, api_client, upload_path, store_key, start_date, end_date=None):
        self.upload_storage = upload_storage
        self.api_client = api_client
        self.upload_path = upload_path
        self.store_key = store_key
        self.start_date = start_date
        self.end_date = end_date or datetime.now().date()
        logger.info('DownloadWorker initialized for %s store key and %s.' % (
            self.store_key, self.start_date.strftime(self.date_format)))

    def _list_existing_dates(self):
        for key in self.upload_storage.list('%s/' % self.upload_path):
            match = FILE_PATTERN_RE.match(key)
            if match:
                date_str = match.groupdict()['date']
                yield datetime.strptime(date_str, self.date_format).date()

    def _generate_date_range(self):
        span = self.end_date - self.start_date
        for i in range(span.days + 1):
            yield self.end_date - timedelta(days=i)

    def get_dates_to_download(self):
        existing_dates = set(self._list_existing_dates())
        full_date_range = set(self._generate_date_range())
        logger.debug('Existing dates: %s' % list(existing_dates))
        return sorted(full_date_range - existing_dates, reverse=True)

    def _generate_params(self, dt):
        return {
            'StoreKey': self.store_key,
            'CheckDate': dt.strftime(self.date_format)
        }

    def _generate_key(self, dt):
        return os.path.join(self.upload_path, '%s.xml' % dt.strftime(self.date_format))

    @backoff.on_exception(
        backoff.expo,
        requests.exceptions.RequestException,
        max_time=retry_timeout_sec,
        max_tries=max_retries,
        giveup=giveup_handler
    )
    def run_for_date(self, date):
        params = self._generate_params(date)
        logger.info('Running worker with params: %s.' % params)
        file = self.api_client.get_store_daily_data(
            params, binary=True)
        if file:
            self.upload_storage.upload(file, self._generate_key(date))
        else:
            logger.warning(
                'No file for params: %s.' % params)

    def run(self):
        dates_to_download = list(self.get_dates_to_download())
        logger.info('Date count to download: %s.' % len(dates_to_download))
        logger.debug('Dates to download: %s' % dates_to_download)
        for date in dates_to_download:
            self.run_for_date(date)
