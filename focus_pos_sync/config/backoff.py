import os


retry_timeout_sec = int(os.environ.get('FOCUS_RETRY_TIMEOUT_SEC') or 600)
max_retries = int(os.environ.get('FOCUS_MAX_RETRIES') or 3)

